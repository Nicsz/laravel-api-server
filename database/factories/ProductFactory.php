<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
      'title' => ucfirst($faker->words(2, true)),
      'price' => $faker->randomFloat(2, 20, 100),
      'description' => $faker->realText(50),
    ];
});
