# Laravel Api Server

This server collect data, which you can get by special endpoint.

## System Requirements

The following are required to function properly.

- Laravel 5.8
- PHP 7.3

## Getting Started

<pre>
git clone https://gitlab.com/Nicsz/laravel-api-server
<span class="pl-c1">cd</span> laravel-api-server
make .env file
php artisan migrate
php artisan db:seed
</pre>


#### Run The Server

in your project root directory, run

``` shell

$ php artisan serve

```

### Configurable Options

The special endpoints in **web.php** file.


| Title            | Default              | Description                      |
| :--------------- | :------------------- | :--------------------------------|
| `allProducts`    | `/api/products`      | The route get all products list  |
| `oneProduct`     | `/api/products/{id}` | The route get one product by id  |
