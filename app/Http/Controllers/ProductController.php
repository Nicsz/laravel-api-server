<?php

namespace App\Http\Controllers;

use App\Product;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Product[]|\Illuminate\Database\Eloquent\Collection
   */
  public function index()
  {
    return Product::all();
  }

  public function show(Product $product)
  {
    return $product = Product::findOrFail($product);
  }
}
